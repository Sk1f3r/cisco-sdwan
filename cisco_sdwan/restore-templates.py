# IMPORT
from typing import Dict
from json import loads, dumps
from pathlib import Path

from shared import log, rest_api_lib
from shared import target_vmanage, username, password
from shared import backup_root
from shared import save_items


class req(rest_api_lib):
    """ A subclass to use methods of rest_api_lib """

    # Inheritance
    def __init__(self, **kwds):
        super().__init__(**kwds)

    def prepare_mappings(self, subdir):
        """ Preparing mappings to be used during DT restoration procedure """
        # Ensure that a provided path exists
        fullpath = Path.joinpath(backup_root, Path(subdir))
        if not fullpath.exists():
            exit("No path exists")

        # as we have nothing to do with default FTs, but DT can use it
        # we have to recieve IDs of all factory FTs presented at target vManage
        ft_summary = loads(self.get_request('template/feature?summary=true'))

        # Extract a valuable part
        fts = ft_summary['data']

        # Preparing a vault for factory templates
        fts_factory = dict()

        # Iter thru fts and copy IDs of factory templates to the vault
        for ft in fts:
            ft_name = ft['templateName']
            ft_id = ft['templateId']
            if "Factory_Default" in ft_name:
                fts_factory[ft_name] = ft_id

        log.debug(f"Got {len(fts_factory)} factory FTs")

        # loading mapping information
        # both original, collected during a backup procedure
        mapping_old = Path.joinpath(fullpath, Path("_ft_mapping_old.json"))
        # and a list of IDs of restored FTs
        mapping_new = Path.joinpath(fullpath, Path("_ft_mapping_new.json"))

        # load-in old mappings from a known file
        with open(mapping_old) as f:
            data = f.read()
            mapping_old = loads(data)
        # load-in new mappings from a known file
        with open(mapping_new) as f:
            data = f.read()
            j_mapping_new = loads(data)
            # merge new mappings with IDs of factory FTs on a target system
            mapping_new = {**j_mapping_new, **fts_factory}

        # store variables inside an object instance
        self.mapping_old = mapping_old
        self.mapping_new = mapping_new

    def restore_ft(self, source_ft):
        """ Restore a FT from a backup """

        # Ensure that a provided path exists
        if not source_ft.exists():
            exit("No path exists")

        with open(source_ft) as f:
            data = f.read()
            # format to JSON
            j_ft = loads(data)

        # extracting a name
        ft_name = j_ft['templateName']

        response = super().post_request(mount_point='template/feature',
                                        payload=j_ft)
        # JSONing a response
        # if there is an error message
        j_result = loads(response)
        if j_result.get('error'):
            # display a body of an error
            print(j_result['error']['details'])
            ft_error = True
        else:
            # else successfully created
            log.info(f"+ {ft_name}")
            ft_error = False

        return ft_name, ft_error, response

    def restore_all_ft(self, subdir):
        """ Restore all FTs from a specified directory """

        # Ensure that a provided path exists
        fullpath = Path.joinpath(backup_root, Path(subdir))
        if not fullpath.exists():
            exit("No path exists")

        new_mapping = dict()

        # Iterate thru pattern-matched files and restore a backup one-by-one
        for ft in Path.glob(fullpath, "ft_*.json"):
            # work with a single entity
            # no interest in factory templates hence skip
            if "Factory" in ft.name:
                continue

            # restoring a single FT
            ft_name, ft_error, result = self.restore_ft(ft)
            # JSONing a response
            j_result = loads(result)
            if not ft_error:
                # extract a new ID to write to a new mapping dict
                new_mapping[ft_name] = j_result['templateId']
                log.info(f"+ {ft_name}")
        if len(new_mapping):
            j_mapping = dumps(new_mapping)
            save_items("_ft_mapping_new.json", j_mapping, subdir=subdir)
        else:
            print("Error: nothing been made")

    def restore_dt(self, source_dt):
        """ Restore a DT from a backup """

        # Ensure that a provided path exists
        if not source_dt.exists():
            exit("No path exists")

        with open(source_dt) as f:
            data = f.read()
            # format to JSON
            j_dt = loads(data)

        # extracting a name
        dt_name = j_dt['templateName']

        # prepare a dict of updated DT and a list to set as 'generalTemplates'
        updated_dt: Dict[str:str] = j_dt
        updated_templates = list()

        # iterate over IDs and update old ID by new
        for template_values in j_dt['generalTemplates']:
            # remember an old template id
            old_id = template_values['templateId']
            # ensure it presented within mapping set of old IDs
            if old_id in self.mapping_old.values():
                # iterate and unpack an ID from old mappings to obtain a name
                for __name, __id in self.mapping_old.items():
                    if old_id == __id:
                        the_name = __name
                        # stop when found
                        break
                # now we can find a new ID by using the found name
                new_id = self.mapping_new.get(the_name)

                # special case when sub-templates exist
                if template_values.get("subTemplates"):
                    # preparing a storage for new subtemplates
                    updated_subtemplates = list()
                    log.debug(f"old sub: {template_values['subTemplates']}")

                    # iterate thri sub-templates
                    for sub in template_values['subTemplates']:
                        # extract sub-id
                        sub_old_id = sub['templateId']
                        # search it in old mappings
                        for __sub_name, __sub_id in self.mapping_old.items():
                            if sub_old_id == __sub_id:
                                # remember a name when found
                                sub_the_name = __sub_name
                                # stop when found
                                break
                        # now we can find a new ID by using the found name
                        new_sub_id = self.mapping_new.get(sub_the_name)
                        # modify a sub
                        sub['templateId'] = new_sub_id
                        # and add to a list of updated subs
                        updated_subtemplates.append(sub)

                    log.debug(f"new sub: {template_values['subTemplates']}")

                log.debug(f"old: {old_id}, new: {new_id}")

                # update a template with new id
                template_values['templateId'] = new_id
                # add to a list of fts
                updated_templates.append(template_values)

        # replace whole section of fts to an updated one
        updated_dt['generalTemplates'] = updated_templates

        # clean up an ID of a centrilized policy as its not available yet
        updated_dt['policyId'] = ''

        # send a request
        response = super().post_request(mount_point="template/device/feature",
                                        payload=updated_dt)

        # analyze a response
        j_result = loads(response)
        # if error
        if j_result.get('error'):
            # display a body of an error
            print(j_result['error']['details'])
            dt_error = True
        # else successfully created
        else:
            log.info(f"+ {dt_name}")
            dt_error = False

        return dt_name, dt_error, response

    def restore_all_dt(self, subdir):
        """ Restore all DTs from a specified directory """

        # Ensure that a provided path exists
        fullpath = Path.joinpath(backup_root, Path(subdir))
        if not fullpath.exists():
            exit("No path exists")

        # Iterate thru pattern-matched files and restore a backup one-by-one
        for dt in Path.glob(fullpath, "dt_*"):
            # work with a single entity
            # restoring a template
            dt_name, dt_error, result = self.restore_dt(dt)

        return None


# MAIN
if __name__ == "__main__":

    session1 = req(address=target_vmanage,
                   username=username,
                   password=password)

    backup_dir = "backup_2019-11-11 - normal"

    single_ft = False
    all_ft = False
    single_dt = False
    all_dt = True

    # restore a single FT
    if single_ft:
        ft = "ft_template_vedge_aaa.json"
        ft_fullname = Path.joinpath(backup_root, backup_dir, ft)
        session1.restore_ft(ft_fullname)

    # restore a set of FTs
    if all_ft:
        session1.restore_all_ft(backup_dir)

    # prepare for DT procedures
    if single_dt or all_dt:
        session1.prepare_mappings(backup_dir)

    # restore a single DT
    if single_dt:
        dt = "dt_template_vedge_site1.json"
        dt_fullname = Path.joinpath(backup_root, backup_dir, dt)
        session1.restore_dt(dt_fullname)

    # restore a set of DTs
    if all_dt:
        session1.restore_all_dt(backup_dir)
