# IMPORT
from json import loads

from shared import rest_api_lib
from shared import source_vmanage, username, password


# CLASS
class req(rest_api_lib):
    """A subclass to use methods of rest_api_lib"""

    # Inheritance
    def __init__(self, **kwds):
        super().__init__(**kwds)

    @property
    def to_get(self):
        pass

    @to_get.getter
    def get_about(self):
        """ To get content of a section About """
        response = loads(self.get_request('client/about'))
        print(response)

    @to_get.getter
    def get_devices(self):
        """ To get a list of devices and basic info (model, site, status) """

        response = loads(self.get_request('device'))
        items = response['data']
        for item in items:
            print(f"""> device: {item['host-name']}
                personality: {item['personality']}
                site-id: {item['site-id']}
                status: {item['status']}
                """)

    def transport_health(self, limit=5):
        response = loads(
            self.get_request('statistics/approute/transport/summary/jitter',
                             limit=limit))
        items = response['data']
        print(len(items))
        # for item in items:
        #     print(f"""color: {item['color']}
        #     jitter: {item['jitter']}
        #     loss_percentage: {item['loss_percentage']}
        #     latency: {item['latency']}""")

    @to_get.getter
    def sites_health(self):
        """ To get a list of sites with health values """

        response = loads(self.get_request('device/bfd/sites/summary'))
        items = response['data'][0]['statusList']
        print(f"{'~'*8} Sites Health {'~'*8}")
        for item in items:
            print(f"""{item['name']}: {item['message'].split()[0]}""")
        print(f"{'-'*29}")

    @to_get.getter
    def tlocutil(self):
        """ To get a list of TLOCs with utilization values """

        response = loads(self.get_request('device/tlocutil'))
        items = response['data']
        print(f"{'~'*5} TLOC Utilization {'~'*5}")
        for item in items:
            if item['value'] > 0:
                print(f"{item['percentageDistribution']}: {item['value']}")
        print(f"{'-'*29}")

    @to_get.getter
    def get_dpi(self):
        """ To get a summary list of recognized applications """
        response = loads(
            self.get_request('statistics/dpi/applications/summary'))
        items = response['data']

        return items


if __name__ == "__main__":
    obj1 = req(address=source_vmanage, username=username, password=password)
    obj1.get_about
    # obj1.get_devices
    # obj1.transport_health(limit=1)
    # obj1.tlocutil
    # obj1.get_dpi
    # obj1.sites_health
