# IMPORT
from json import loads, dumps

from shared import log, rest_api_lib
from shared import source_vmanage, username, password
from shared import save_items


# CLASS
class req(rest_api_lib):
    """A subclass to use methods of rest_api_lib"""

    # Inheritance
    def __init__(self, **kwds):
        super().__init__(**kwds)

    def get_all_ft(self, no_default=False):
        """ Get a list of feature templates """

        # Requesting an make it to be a dict
        response = loads(self.get_request('template/feature?summary=true'))

        # Extract a valuable part
        fts = response['data']

        if no_default:
            # Preparing a vault for non-default templates
            fts_no_factory = []

            # Iter thru fts and copy non-default to the vault
            for ft in fts:
                if "Factory_Default" not in ft['templateName']:
                    fts_no_factory.append(ft)

            print(
                f"Got {len(fts_no_factory)} feature templates without defaults"
            )
            return fts_no_factory

        print(f"Got {len(fts)} feature templates")

        return fts

    def get_all_dt(self):
        """ Get a list of device templates """

        # Requesting an make it to be a dict
        response = loads(self.get_request('template/device'))

        # Extract a valuable part
        dts = response['data']

        print(f"Got {len(dts)} device templates")

        return dts

    def get_specific_template(self, type=None, id=None):
        """ Get a specific template object by ID """

        # Request a specific device template
        if type == "dt":
            obj_data = self.get_request(f'template/device/object/{id}')
        elif type == "ft":
            obj_data = self.get_request(f'template/feature/object/{id}')
        else:
            exit(255)

        # Extract a name to pass separately
        obj_name = loads(obj_data)["templateName"]

        return obj_name, obj_data


# MAIN
if __name__ == "__main__":
    # The session
    session1 = req(address=source_vmanage,
                   username=username,
                   password=password)

    # saving all FTs, one by one
    backup_fts = True
    fts = session1.get_all_ft()
    if backup_fts and len(fts):
        ft_mapping = dict()
        for ft in fts:
            id = ft["templateId"]
            name, ft = session1.get_specific_template(id=id, type="ft")
            save_items(f"ft_{name}.json", ft)
            ft_mapping[f'{name}'] = str(id)
        save_items("_ft_mapping_old.json", dumps(ft_mapping))

    # saving all DTs, one by one
    backup_dts = True
    dts = session1.get_all_dt()
    if backup_dts and len(dts):
        for dt in dts:
            id = dt["templateId"]
            name, dt = session1.get_specific_template(id=id, type="dt")
            save_items(f"dt_{name}.json", dt)
