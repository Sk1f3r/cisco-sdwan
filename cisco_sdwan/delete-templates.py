# IMPORT
from json import loads

from shared import log, rest_api_lib
from shared import source_vmanage, target_vmanage, username, password


# CLASS
class req(rest_api_lib):
    """A subclass to use methods of rest_api_lib"""

    # Inheritance
    def __init__(self, **kwds):
        super().__init__(**kwds)

    def get_all_ft(self):
        """ Get a list of feature templates """

        # Requesting an make it to be a dict
        response = loads(self.get_request('template/feature?summary=true'))

        # Extract a valuable part
        fts = response['data']

        # Preparing a vault for non-default templates
        fts_no_factory = []

        # Iter thru fts and copy non-default to the vault
        for ft in fts:
            if "Factory_Default" not in ft['templateName']:
                fts_no_factory.append(ft)

        print(f"Got {len(fts_no_factory)} feature templates without defaults")

        return fts_no_factory

    def get_all_dt(self):
        """ Get a list of device templates """

        # Requesting an make it to be a dict
        response = loads(self.get_request('template/device'))

        # Extract a valuable part
        dts = response['data']

        print(f"Got {len(dts)} device templates")

        return dts

    def rm_specific_template(self, type=None, id=None):
        """ Remove a specific template object by ID """

        # Request a specific device template
        if type == "dt":
            obj_data = self.delete_request(f'template/device/{id}')
        elif type == "ft":
            obj_data = self.delete_request(f'template/feature/{id}')
        else:
            exit("Unknown object type or a type wasn't provided at all")

        # Extract a name to pass separately
        return obj_data


# MAIN
if __name__ == "__main__":
    # The session
    session1 = req(address=target_vmanage,
                   username=username,
                   password=password)

    # delete all DTs, one by one
    dts = session1.get_all_dt()
    if len(dts):
        for dt in dts:
            id = dt["templateId"]
            name = dt["templateName"]
            result = session1.rm_specific_template(id=id, type="dt")
            if not result:
                log.info(f"- {name} deleted")

    # delete all FTs, one by one
    fts = session1.get_all_ft()
    if len(fts):
        for ft in fts:
            id = ft["templateId"]
            name = ft["templateName"]
            result = session1.rm_specific_template(id=id, type="ft")
            if not result:
                log.info(f"- {name} deleted")
