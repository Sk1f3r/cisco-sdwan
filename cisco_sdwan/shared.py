# IMPORT
import logging

from requests import packages, session
from sys import exit
from json import dumps
from pathlib import Path
from datetime import date
from requests.packages.urllib3.exceptions import InsecureRequestWarning
packages.urllib3.disable_warnings(InsecureRequestWarning)

# VAR
source_vmanage = "vmanage101.csdwan.lab"
target_vmanage = "vmanage101.csdwan.lab"
username, password = "admin", "admin"
content_json = {'Content-Type': 'application/json'}
backup_root = Path("backups")

# LOGGING
FORMAT = '%(asctime)s | %(levelname)s | %(message)s'
logging.basicConfig(format=FORMAT)
log = logging.getLogger(__name__)
log.setLevel(20)


# FUNC
def save_items(filename, content, subdir=None):
    """ Save a passed content to specified file

    Arguments:
        filename {[str]} -- [a name of a target file]
        content {[str]} -- [a string to be written to a file]

    Returns:
        [None] -- [nothing to return]
    """

    if not subdir:
        # a date to qualify a name
        day = date.today()

        # Creating a directory of the day
        dated_dir = Path(f"backup_{day}")
        full_dir = Path.joinpath(backup_root, dated_dir)
        full_dir.mkdir(exist_ok=True, parents=True)
    else:
        full_dir = Path.joinpath(backup_root, subdir)

    # If fine, open a file and write it down
    if full_dir.exists():
        full_path = Path.joinpath(full_dir, filename)
        with open(full_path, 'w') as f:
            size = f.write(str(content))
            if size:
                log.info(f"+ {full_path.name}")
    return None


# CLASS
class rest_api_lib:
    '''
    Main class from official documentation to establish a connection
    and for a url for basic GET and POST requests.
    If any changes had to be done: change class req()
    '''

    #
    def __init__(self, address, username, password, **kwds):
        """Has to be initialized with host/login/password

        Arguments:
            address {[str]} -- [hostname or IP of a vManage instance]
            username {[str]} -- [username to use]
            password {[str]} -- [password to use]
        """
        self.address = address
        self.session = {}
        self.login(self.address, username, password)
        super().__init__(**kwds)

    # Logging in a system
    def login(self, address, username, password):
        """Logging in to a vManage instance

        Arguments:
            address {[str]} -- [hostname or IP of a vManage instance]
            username {[str]} -- [username to use]
            password {[str]} -- [password to use]

        Returns:
            [None] -- [nothing to return]
        """
        base_url_str = f'https://{address}/'
        login_action = 'j_security_check'
        token_action = 'dataservice/client/token'

        # Format data for loginForm
        login_data = {'j_username': username, 'j_password': password}

        # Url for posting login data
        login_url: str = base_url_str + login_action
        token_url: str = base_url_str + token_action

        # Making a session object
        sess: object(rest_api_lib) = session()

        # Establishing
        login_response = sess.post(url=login_url,
                                   data=login_data,
                                   verify=False)

        # If an answer starts as html page then login procudeure has failed
        if b'<html>' in login_response.content:
            print("Login Failed")
            exit(0)

        # Updating token to use more
        login_token: str = sess.get(url=token_url, verify=False)

        # Check if there is any content at all
        if login_token.status_code == 200:
            if b'<html>' in login_token.content:
                print('Token Failed')
                exit(1)
            sess.headers['X-XSRF-TOKEN'] = login_token.content
            self.session[address] = sess

        return None

    def get_request(self, mount_point, **params):
        """A template of a basic GET request

        Arguments:
            mount_point {[str]} -- [specific URI path]

        Returns:
            [Dict] -- [a dictionary of a JSON response]
        """
        url = f"https://{self.address}:8443/dataservice/{mount_point}"
        values = params if params else None
        response = self.session[self.address].get(url,
                                                  verify=False,
                                                  params=values)

        # Make it to be a string of pure JSON
        result = dumps(response.json())

        return result

    def post_request(self, mount_point, payload=None, headers=content_json):
        """A template of a basic POST request

        Arguments:
            mount_point {[str]} -- [specific URI path]
            payload {[dict]} -- [a dictionary of a JSON request]

        Keyword Arguments:
            headers {[Dict]} -- [a variable to mark] (default: {content_json})

        Returns:
            [type] -- [description]
        """
        url = f"https://{self.address}:8443/dataservice/{mount_point}"
        payload = dumps(payload)
        response = self.session[self.address].post(url=url,
                                                   data=payload,
                                                   headers=headers,
                                                   verify=False)
        return response.content

    def delete_request(self, mount_point, payload=None, headers=content_json):
        """A template of a basic DELETE request

        Arguments:
            mount_point {[str]} -- [specific URI path]
            payload {[dict]} -- [a dictionary of a JSON request]

        Keyword Arguments:
            headers {[Dict]} -- [a variable to mark] (default: {content_json})

        Returns:
            [type] -- [description]
        """
        url = f"https://{self.address}:8443/dataservice/{mount_point}"
        payload = dumps(payload)
        response = self.session[self.address].delete(url=url,
                                                     data=payload,
                                                     headers=headers,
                                                     verify=False)
        return response.content
